<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>ApplicationDelegate</name>
    <message>
        <location filename="../ui/ApplicationDelegate.qml" line="404"/>
        <source>GET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDelegate.qml" line="418"/>
        <source>INSTALL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDelegate.qml" line="456"/>
        <source>REMOVE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationDetail</name>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="704"/>
        <source>no screenshot found!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="472"/>
        <source>Disclaimer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="372"/>
        <location filename="../ui/ApplicationDetail.qml" line="1301"/>
        <source>ratings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="131"/>
        <source>years</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="127"/>
        <source>months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="123"/>
        <source>days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="119"/>
        <source>hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="115"/>
        <source>minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="111"/>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="359"/>
        <source>Reminder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="360"/>
        <source>First, you have to install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="361"/>
        <source>Then, you can vote.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="362"/>
        <source>ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="372"/>
        <location filename="../ui/ApplicationDetail.qml" line="1301"/>
        <source>not enough rating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="387"/>
        <source>Your rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="473"/>
        <source>This application served from Pardus non-free package repositories, so that the OS has nothing to do with the health of the application. Install with caution.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="501"/>
        <source>remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="501"/>
        <source>install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="600"/>
        <source>OPEN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="761"/>
        <source>more</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="844"/>
        <source>website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="1116"/>
        <source>Available Repositories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="1329"/>
        <source>last applied review</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="1452"/>
        <source>no changelog data found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="1391"/>
        <source>changelog history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="902"/>
        <source>e-mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="931"/>
        <source>information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="1508"/>
        <source>version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="964"/>
        <source>Download size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="989"/>
        <source>type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="997"/>
        <source>non-free</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="997"/>
        <source>open source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="1014"/>
        <source>category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="1052"/>
        <source>license</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="1091"/>
        <source>Download count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="1152"/>
        <source>reviews - ratings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="1343"/>
        <source>coming soon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="1376"/>
        <source>what is new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ApplicationDetail.qml" line="744"/>
        <source>no description found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationList</name>
    <message>
        <location filename="../ui/ApplicationList.qml" line="26"/>
        <source>no match found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfirmationDialog</name>
    <message>
        <location filename="../ui/ConfirmationDialog.qml" line="76"/>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ConfirmationDialog.qml" line="83"/>
        <source>no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ConfirmationDialog.qml" line="104"/>
        <source>Are you sure you want to remove this application ?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Home</name>
    <message>
        <location filename="../ui/Home.qml" line="257"/>
        <source>welcome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="365"/>
        <source>Editor&apos;s Pick</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="383"/>
        <location filename="../ui/Home.qml" line="567"/>
        <location filename="../ui/Home.qml" line="751"/>
        <source>no valid data found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="548"/>
        <source>Most Downloaded App</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="732"/>
        <source>Most Rated App</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1005"/>
        <source>create option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1030"/>
        <source>after survey will be ended</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1040"/>
        <source>days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1043"/>
        <source>hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1046"/>
        <source>minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1049"/>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1131"/>
        <source>Application Request From for Survey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1187"/>
        <source>This property is going to be used for differentiate the options of the survey. Use &quot;-&quot; for separations between words instead of &quot; &quot; (space) because single line words will be accepted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1201"/>
        <source>Application Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1212"/>
        <location filename="../ui/Home.qml" line="1270"/>
        <location filename="../ui/Home.qml" line="1328"/>
        <location filename="../ui/Home.qml" line="1386"/>
        <location filename="../ui/Home.qml" line="1446"/>
        <source>e.g.:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1244"/>
        <source>Breafly describe why do we need this application in one sentence.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1258"/>
        <source>Reason</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1270"/>
        <source>This application going to be usefull for highschools.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1302"/>
        <source>A working website that we can gather information from.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1316"/>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1360"/>
        <source>We are going to use your e-mail address for communication. Dummy inputs will be banned automatically.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1374"/>
        <source>E-mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1418"/>
        <source>This property holds detailed explanations as rich text. You can paste prepared content here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1432"/>
        <source>Explanation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1447"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1448"/>
        <location filename="../ui/Home.qml" line="1450"/>
        <source>paragraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1449"/>
        <source>Another title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1541"/>
        <source>The content that you have filled, will be send to main server to be exemined. Any inappropriate content assumed as ban couse. Do you want to proceed ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1553"/>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1560"/>
        <source>no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Home.qml" line="1499"/>
        <source>send</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../ui/InfoDialog.qml" line="58"/>
        <source>settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/InfoDialog.qml" line="7"/>
        <location filename="../ui/InfoDialog.qml" line="93"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/InfoDialog.qml" line="91"/>
        <source>Something went wrong!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <location filename="../ui/NavigationBar.qml" line="89"/>
        <source>all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/NavigationBar.qml" line="294"/>
        <source>This section is under development</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/NavigationBar.qml" line="332"/>
        <source>Anonymus Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/NavigationBar.qml" line="377"/>
        <source>is</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QueueDialog</name>
    <message>
        <location filename="../ui/QueueDialog.qml" line="37"/>
        <source>queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/QueueDialog.qml" line="84"/>
        <source>remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/QueueDialog.qml" line="84"/>
        <source>install</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchBar</name>
    <message>
        <location filename="../ui/SearchBar.qml" line="122"/>
        <source>Search an application</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../ui/Settings.qml" line="58"/>
        <source>enable animations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="201"/>
        <source>update package manager cache on start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="241"/>
        <source>Correct package manager sources list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="268"/>
        <source>correct</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="268"/>
        <source>corrected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="290"/>
        <location filename="../ui/Settings.qml" line="632"/>
        <source>Clear cache files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="317"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="339"/>
        <source>use dark theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="394"/>
        <location filename="../ui/Settings.qml" line="458"/>
        <source>Informing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="409"/>
        <source>I have carefully read the explanation on the right and agree to proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="417"/>
        <location filename="../ui/Settings.qml" line="481"/>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="423"/>
        <location filename="../ui/Settings.qml" line="487"/>
        <source>no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="473"/>
        <source>I read the explanation on the right and agree to proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="627"/>
        <source>Controls the transitions animations. If you have low performance graphic card, disabling animation may give you more comfort.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="629"/>
        <source>Checks the system package manager&apos;s cache when Pardus-Store is started. Disabling this could speed up the starting process but if you do not use Pardus-Store often you should enable this option.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="630"/>
        <source>Corrects the system sources list that used by package manager. This process will revert all the changes have been done and will use Pardus Official Repository source addresses. Use with caution.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="631"/>
        <source>Change theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/Settings.qml" line="628"/>
        <source>Controls the visual column count of applications list view.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../ui/SplashScreen.qml" line="111"/>
        <source>Updating package manager cache.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SurveyDetail</name>
    <message>
        <location filename="../ui/SurveyDetail.qml" line="61"/>
        <source>Vote count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/SurveyDetail.qml" line="133"/>
        <source>Why we should vote for this ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/SurveyDetail.qml" line="158"/>
        <source>Detailed explanation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/SurveyDetail.qml" line="192"/>
        <source>Where can we be more informed about this ?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../ui/main.qml" line="14"/>
        <location filename="../ui/main.qml" line="159"/>
        <location filename="../ui/main.qml" line="476"/>
        <source>Store</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="30"/>
        <source>Something went wrong!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="37"/>
        <source>home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="37"/>
        <source>categories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="37"/>
        <source>settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="234"/>
        <source>Fetching survey data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="262"/>
        <location filename="../ui/main.qml" line="269"/>
        <source>Informing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="277"/>
        <location filename="../ui/main.qml" line="284"/>
        <source>Show this result to the maintainer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="539"/>
        <source>Fetching application list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="157"/>
        <source>Package process is complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="177"/>
        <source>Another application is using package manager. Please wait or discard the other application and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="180"/>
        <source>Pardus Store should be run with root privileges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="182"/>
        <source>Pardus Store detected some broken sources for the package manager.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="182"/>
        <source>Please fix it manually or use Pardus Store&apos;s settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="439"/>
        <source>Removing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="437"/>
        <source>Downloading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="438"/>
        <source>Installing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="221"/>
        <source>Reason</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="222"/>
        <source>Suggestion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="223"/>
        <source>Check your internet connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="229"/>
        <source>Gathering local details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="242"/>
        <source>Done.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="263"/>
        <source>Correcting of system package manager sources list is done. You can now restart Pardus Store.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="270"/>
        <source>Cache files cleared successfully.You can now restart Pardus Store.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="435"/>
        <source>Installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="436"/>
        <source>Removed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="475"/>
        <source>Warning!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="476"/>
        <source>can not be closed while a process is ongoing.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
